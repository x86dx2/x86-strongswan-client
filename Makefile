# Makefile for StrongSwan Configuration and Management
# Author: Nelson Junior
# Email: nelson.junior@x86.com.br
# Site: x86.com.br
# Git: https://gitlab.com/x86dx2/ | https://github.com/x86dx2
# Date: 2023-12-31 23:59
# Version: 1.0

.PHONY: ansible-install strongswan-install-repo strongswan-build-source config-clone config-install start stop restart status statusall connect disconnect listconn listcacerts cleanup help

STRONGSWAN_VERSION = 5.9.13
PYTHON_VERSION = 3.11
SERVICE_NAME = ipsec
	# Add the repository to Apt sources:
help:
	@echo "Uso: make [ansible-install strongswan-install-repo|strongswan-build-source|start|stop|restart|status|statusall|connect|disconnect|listconn|listcacerts|cleanup|help]"

docker-install:
	@sudo apt-get update
	@sudo apt-get install -y ca-certificates curl gnupg
	@sudo install -m 0755 -d /etc/apt/keyrings
	@sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
	@echo "deb [arch=$(shell dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(shell . /etc/os-release && echo "$$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
	@sudo apt-get update
	@sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
	@sudo usermod -aG docker $$USER

ansible-install:
	@sudo add-apt-repository -y "deb http://archive.ubuntu.com/ubuntu $$(lsb_release -sc) universe"
	@sudo apt update #&& sudo apt upgrade -y
	@sudo apt install \
		python$(PYTHON_VERSION)-minimal \
		python$(PYTHON_VERSION)-venv \
		pipx
	@pipx install ansible-core --python /usr/bin/python$(PYTHON_VERSION)
	@pipx ensurepath
	@export PATH="$$HOME/.local/bin:$$PATH"; ansible-galaxy collection install community.general

strongswan-install-repo:
	@sudo apt update && sudo apt upgrade -y
	@sudo apt install -y \
		strongswan \
		strongswan-charon \
		strongswan-libcharon \
		strongswan-starter \
		libcharon-extauth-plugins \
		libcharon-extra-plugins \
		libstrongswan \
		libstrongswan-extra-plugins \
		libstrongswan-standard-plugins

strongswan-build-source:
	@sudo apt update && sudo apt upgrade -y
	@sudo apt install -y \
		wget \
		bzip2 \
		libssl-dev \
		gcc \
		make
	@wget https://download.strongswan.org/strongswan-$(STRONGSWAN_VERSION).tar.bz2
	@tar xjvf strongswan-$(STRONGSWAN_VERSION).tar.bz2 -C ~/.cache/
	cd ~/.cache/strongswan-$(STRONGSWAN_VERSION) && \
	./configure --prefix=/usr --sysconfdir=/etc \
		--disable-gmp \
		--enable-vici \
		--enable-openssl \
		--enable-eap-identity \
		--enable-eap-md5 \
		--enable-eap-tls \
		--enable-eap-ttls \
		--enable-eap-peap \
		--enable-eap-dynamic && \
	make -j && \
	sudo make install
	@rm -rf ./strongswan-*
	
config-clone:
	git clone https://gitlab.com/x86dx2/x86-strongswan-client.git
	@echo ""
	@echo "### ATENTION ###"
	@echo ""
	@echo "Update username e password in the following files:"
	@echo "eap_identity=USERNAME	 - /etc/ipsec.conf"
	@echo "USERNAME : EAP "PASSWORD" - /etc/ipsec.secrets"

config-install:
	@echo "Making backups..."
	@install -b -m 644 /etc/ipsec.conf /etc/ipsec.conf.$$(date '+%Y%m%d%H%M%S')
	@install -b -m 600 /etc/ipsec.secrets /etc/ipsec.secrets.$$(date '+%Y%m%d%H%M%S')
	@install -b -m 644 /etc/strongswan.conf /etc/strongswan.conf.$$(date '+%Y%m%d%H%M%S')
	@echo "Copying new configuration files..."
	@install -m 644 conf/ipsec.conf /etc/ipsec.conf
	@install -m 600 conf/ipsec.secrets /etc/ipsec.secrets
	@install -m 644 conf/strongswan.conf /etc/strongswan.conf
	@install -m 644 cacerts/* /etc/ipsec.d/cacerts/
	@echo "Configuration files installed successfully."

start:
	sudo @$(SERVICE_NAME) start

stop:
	sudo @$(SERVICE_NAME) stop

restart: 
	sudo @$(SERVICE_NAME) restart

status:
	sudo @$(SERVICE_NAME) status

statusall:
	sudo @$(SERVICE_NAME) statusall

connect:
	sudo @$(SERVICE_NAME) up $(word 2,$(MAKECMDGOALS))
	
disconnect: 
	sudo @$(SERVICE_NAME) down $(word 2,$(MAKECMDGOALS))

listconn:
	sudo @$(SERVICE_NAME) statusall | sed -n '/Connections/,/Security Associations/ {/Security Associations/!p}'

listcacerts:
	sudo @$(SERVICE_NAME) listcerts

cleanup:
	@if command -v ipsec >/dev/null; then \
		if dpkg -l | grep -q strongswan; then \
			echo "Removing installation by repository..."; \
			sudo apt remove --purge -y \
				strongswan \
				strongswan-charon \
				strongswan-libcharon \
				strongswan-starter \
				libcharon-extauth-plugins \
				libcharon-extra-plugins \
				libstrongswan \
				libstrongswan-extra-plugins \
				libstrongswan-standard-plugins \
		elif [ -e ~/.cache/strongswan-$(STRONGSWAN_VERSION)/Makefile ]; then \
			echo "Removing installation built by source..."; \
			cd ~/.cache/strongswan-$(STRONGSWAN_VERSION) && sudo make uninstall; \
		fi; \
		sudo rm -rf /etc/ipsec* 2>/dev/null || true; \
		sudo rm -rf /etc/strongswan* 2>/dev/null || true; \
		sudo rm -rf ~/.cache/strongswan-* 2>/dev/null || true; \
		sudo rm -rf /usr/share/strongswan* 2>/dev/null || true; \
		echo "Finished cleaning."; \
	else \
		echo "StrongSwan-$(STRONGSWAN_VERSION) not installed."; \
		exit 0; \
	fi