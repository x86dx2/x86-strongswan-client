#!/bin/bash

apt-get update
apt-get install ca-certificates curl
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null

apt-get update && apt-get install --yes \
    git \
    make \
    strongswan \
    strongswan-starter \
    strongswan-charon \
    strongswan-libcharon \
    libcharon-extauth-plugins \
    libcharon-extra-plugins \
    libstrongswan \
    libstrongswan-extra-plugins \
    libstrongswan-standard-plugins \
    docker-ce \
    docker-ce-cli \
    containerd.io \
    docker-buildx-plugin \
    docker-compose-plugin

files_to_remove=(
    "/etc/ipsec.conf"
    "/etc/ipsec.secrets"
    "/etc/strongswan.conf"
)
for file in "${files_to_remove[@]}"; do
    rm -rf "$file"
done

files_to_install=(
    "/vagrant/conf/ipsec.conf:/etc/ipsec.conf"
    "/vagrant/conf/ipsec.secrets:/etc/ipsec.secrets"
    "/vagrant/conf/strongswan.conf:/etc/strongswan.conf"
    "/vagrant/cacerts/al.mt.gov.br.crt:/etc/ipsec.d/cacerts/al.mt.gov.br.crt"
)
for file in "${files_to_install[@]}"; do
    source_file="${file%%:*}"
    destination="${file#*:}"
    if [ ! -d "$(dirname "$destination")" ]; then
        mkdir -p "$(dirname "$destination")"
    fi
    install -m 644 "$source_file" "$destination"
done