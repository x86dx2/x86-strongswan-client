#!/usr/bin/expect -f

exp_internal 0
proc execute_commands {commands} {
    foreach {command sleep_time} $commands {
        expect {
            -re {x86@.*\$ $} {}
            -re {vagrant@.*\$ $} {}
        }
        send -- "$command\r"
        if {[string match "sudo *" $command]} {
            expect {
                "assword" {
                    send -- "12345678\r"
                    exp_continue
                }
                -re {x86@.*\$ $} {}
                -re {vagrant@.*\$ $} {}
            }
        }
        expect {
            -re {x86@.*\$ $} {}
            -re {vagrant@.*\$ $} {}
        }
        sleep $sleep_time
        send -- "\r"
    }
}
set commands {
    "cat /etc/os-release" 1
    "git clone https://gitlab.com/x86dx2/x86-strongswan-client.git || true" 1
    "cd x86-strongswan-client/ && ls -la" 1
    "cd vagrant-debian/ && ls -la" 1
    "sudo vagrant up" 1
    "sudo vagrant ssh" 1
    "cat /etc/os-release" 0
    "ping 192.168.21.193 -c 1" 0
    "sudo ipsec start" 1
    "sudo ipsec up almt" 0
    "ping 192.168.21.193 -c 5" 0
    "sudo ipsec down almt" 0
    "sudo ipsec stop" 0
    "ping 192.168.21.193 -c 1" 0
    "cd /vagrant && ls -la" 0
    "sudo docker compose up -d" 0
    "sudo docker compose logs | grep established" 0
    "ping 192.168.21.193 -c 5" 0
    "sudo docker compose down" 0
    "ping 192.168.21.193 -c 1" 0
    "exit" 0
    "sudo vagrant halt" 1
}
spawn bash
execute_commands $commands
expect eof
