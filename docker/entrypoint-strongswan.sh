#!/bin/bash

ipsec start --nofork &

sleep 3

if [ -n "$VPN_SITE" ]; then
    exec ipsec up $VPN_SITE &
fi

while true; do
    sleep 60
done
