# x86 strongSwan Client

x86 strongSwan Client is an adaptation of the strongSwan project aimed
at VPN connection with Check Point Gateway firewalls on Ubuntu 22.04
Linux systems.

With the provided configuration, the StrongSwan client will be ready
for VPN connections with the following authentication methods:

* **EAP-GTC:** EAP authentication method based on generic passwords, useful for simple password authentication.
* **EAP-Identity*:** Identity-based authentication method.
* **EAP-MD5:** MD5 Challenge Handshake Authentication Protocol (CHAP) authentication method.
* **EAP-TLS:** Certificate-based TLS EAP authentication method.
* **EAP-TTLS:** EAP tunnels within TLS, providing secure authentication.
* **EAP-PEAP:** Protected Extensible Authentication Protocol, an EAP method encapsulated in a TLS layer.
* **EAP-Dynamic:** Enables dynamic EAP authentication, offering flexibility in the choice of EAP method.

These methods cover a variety of authentication scenarios, providing
flexibility and security for different environments and requirements.

The original project can be accessed at <https://www.strongswan.org/>

## Installing from Distro

A pre-built Libreswan package is available on the following OS
distributions: Ubuntu.

Unless a source-based build is truly needed,  it is often best to use
the pre-built version of the distribution you are using.

For Ubuntu

    apt install -y \
        strongswan \
        strongswan-charon \
        strongswan-libcharon \
        strongswan-starter \
        libcharon-extauth-plugins \
        libcharon-extra-plugins \
        libstrongswan \
        libstrongswan-extra-plugins \
        libstrongswan-standard-plugins

PS: Pre-built packages are generally not in the latest version.

## Installing from Source

### Requirements

There are a few packages required for strongSwan to compile from source:

For Ubuntu

    apt install -y wget bzip2 libssl-dev gcc make

### Building and Installing

Download the latest version of the StrongSwan source file, unzip the file,
access the directory, configure, compile and install.
Follow the commands below:

    STRONGSWAN_VERSION=5.9.13
    wget https://download.strongswan.org/strongswan-$(STRONGSWAN_VERSION).tar.bz2
    tar xjvf strongswan-$(STRONGSWAN_VERSION).tar.bz2
    cd strongswan-$(STRONGSWAN_VERSION)
    systemctl start ipsec.service
    ./configure --prefix=/usr --sysconfdir=/etc \
        --disable-gmp \
        --enable-vici \
        --enable-openssl \
        --enable-eap-identity \
        --enable-eap-md5 \
        --enable-eap-tls \
        --enable-eap-ttls \
        --enable-eap-peap \
        --enable-eap-dynamic
    make -j
    make install

## Starting strongSwan

The installation will detect the init system used (systemd, upstart,
sysvinit, openrc) and must integrate with the Linux distribution.
The name of the service is called `ipsec`. For example, in Ubuntu 22.04,
someone would use:

    systemctl enable ipsec.service
    systemctl start ipsec.service

If unsure of the specific init system used on the system, the `ipsec`
command can also be used to start or stop the ipsec service.  This
command will auto-detect the init system and invoke it:

    ipsec start
    ipsec stop

## Management and Status

For start the service, use:

    ipsec start

For stop the service, use:

    ipsec stop

For restart the service, use:

    ipsec restart

For a brief status overview, use:

    ipsec status

For a connection status overview, use:

    ipsec statusall

For list all available connections, use:

    ipsec statusall | sed -n '/Connections/,/Security Associations/ {/Security Associations/!p}'

For list all uploaded certificates, use:

    ipsec listcerts

## Configuration

Most of the strongSwan configuration is stored in `/etc/ipsec.conf`,
`/etc/ipsec.secrets` and `/etc/strongswan.conf`. Include files may be
present in `/etc/ipsec.d/` and `/etc/strongswan.d/`.
See the respective man pages for more information.

Edit the `/etc/ipsec.conf`` file and enter the username, vpn address
and certificate ID:

        eap_identity=<USERNAME>        # ex: jonh.doe
        right=<VPN ADDRESS>            # ex: vpn.x86.com.br | X.X.X.X
        rightid=<ID CERTIFICATE>       # ex: "O=checkpoint, CN=CLUSTER"

Edit the `/etc/ipsec.secrets` file and enter the username and password:

        <USERNAME> : EAP "<PASSWORD>"  # ex: jonh.doe : EAP "12345678"

## Troubleshooting

Store all public certificates in `/etc/ipsec.d/cacerts/` and type the
following command to get the certificate ID:

        RUN
        ipsec listcerts
        
        OUTPUT
        List of X.509 End Entity Certificates
        subject:  "O=checkpoint-sms.al.mt.gov.br.kc6zw7, CN=CLUSTER-EXT VPN Certificate"

The subject field contains the value that should be configured in
`rightid` for connection in `/etc/ipsec.conf`.

## Documentation

The most up to date documentation consists of the man pages that come
with the software. Further documentation can be found at
[**StrongSwan Project**](https://docs.strongswan.org/) and
[**Check Point Remote Access VPN R81.20**](https://sc1.checkpoint.com/documents/R81.20/WebAdminGuides/EN/CP_R81.20_RemoteAccessVPN_AdminGuide/Content/Topics-VPNRG/strongSwan-Client-Support.htm)
